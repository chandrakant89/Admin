<%@include file="webResource.jsp"%>
<%@include file="navbar.jsp"%> 
<%@include file="leftmenu.jsp"%>		
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
				<li class="active">addEmployee</li>
				
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Add Employee Detail</h1>
			</div>
		</div><!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add New Employee Basic and Professional details</div>
				<div class="panel-body">
				<div class="${cls}">
						<div class="${sbmsg}"><strong>${message}</strong></div>
					</div>
					<form role="form" action="EmployeeServlet" method="post" enctype="multipart/form-data">
						<div class="col-md-4">
							<div class="form-group">
								<label>Name</label> <input class="form-control" type="text" name="name" maxlength="15"
									placeholder="Please Enter Name" required>
							</div>
							<div class="form-group">
								<label>Date Of Birth</label> <input name="dateOfBirth" id="addempCal" maxlength="15"
									class="form-control" placeholder="yyyy-mm-dd" required>
							</div>
							<div class="form-group">
								<label>Gender</label> 
								<input type = "radio" name="gender" id="male" value = "male" required>Male
								<input type = "radio" name="gender" id="female" value = "female" required>Female
							</div>
							
							<div class="form-group">
								<label>Mobile No</label> <input type="text" name="mobNo" maxlength="10" onkeypress="javascript:return isNumber(event)"
									class="form-control" placeholder="Please Enter Mobile No" required>
							</div>
							<div class="form-group">
								<label>Address</label> <input type="text" class="form-control" name="address" maxlength="50"
									placeholder="Please Enter Address" required>
							</div>
						
							<div class="form-group">
								<label>Designation</label> <input type="text" class="form-control" name="designation" maxlength="50"
									placeholder="Please Enter Designation" required>
							</div>
							
							
							<div class="form-group">
								<label>Email Id</label> <input type="email" class="form-control" name="emailId" maxlength="50"
									placeholder="Please Enter Email Id" required>
							</div>
						
						</div>	
						<div class="col-md-4">
							<div class="form-group">
								<label>Employee Job Type</label>
								<select name="empJobType" id="empJobType" class="form-control">
									<option value="">--Select Employee Job Type --</option>
									<option value="fullTime">Full Time</option>
									<option value="partTime">Part Time</option>
								</select>
							</div>
							
							<div class="form-group">
								<label>Date Of Hire</label> <input class="form-control" name="dateOfHire" id="hirempCal" onkeypress="javascript:return isNumber(event)"
									placeholder="yyyy-mm-dd" required>
							</div>
							
							<div class="form-group">
								<label>Marital Status</label> 
								<select name="maritalStatus" id="maritalStatus" class="form-control">
									<option value="">--Please Select Marital Status --</option>
									<option value="married">Married</option>
									<option value="unmarried">Unmarried</option>
									<option value="divorcee">Divorcee</option>
									<option value="complicated">Complicated</option>
								</select>
							</div>
							
							<div class="form-group">
								<label>Status</label>
								<select name="status" id="status" class="form-control">
									<option value="">--Please Select Status --</option>
									<option value="active">Active</option>
									<option value="inactive">In Active</option>
									</select>
							</div>
							
							<div class="form-group">
								<label>Emergency Mobile No</label> <input type="text" name="emergencyMobNO" maxlength="10"
									class="form-control" placeholder="Please Enter Emergency Mobile No" required>
							</div>
							
							<div class="form-group">
								<label>Pan No</label> <input type="text" class="form-control" name="panNo" maxlength="10"
									placeholder="Please Enter Pan No." required>
							</div>
				
							<div class="form-group">
								<label>Adhar No</label> <input type="text" class="form-control" name="adharNo" maxlength="14"
									placeholder="Please Enter Adhar no." required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Photo</label><br>
								<img id="blah" src="image/user.png" class="img-profile"/>
							</div>
							<div class="form-group">
								<input type="file" name="photo" id = "photo" onchange="readURL(this);" accept="image/png, image/jpeg">
							</div>
							
						</div>
						<div class="col-md-10">
							<div class="form-group text-center">
								<input type="submit" class="btn btn-lg btn-primary" value="Submit"/>
							</div>
						</div> 
					</form>
				</div>
			</div>
		</div>
		<!-- /.panel-->
	</div>

	</div>	<!--/.main-->
	<%@include file="footer.jsp"%>
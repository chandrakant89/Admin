<%@include file="webResource.jsp"%>
<%@include file="navbar.jsp"%>
<%@include file="leftmenu.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"> <em class="fa fa-home"></em>
			</a></li>
			<li class="active">Dashboard</li>
			<li class="active">AddJobs</li>
		</ol>
	</div>
	<!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Jobs</h1>
		</div>
	</div>
	<!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add detail for new job Requirement</div>
				<div class="panel-body">
					<form role="form" action="jobPostServlet" method="post">
						<div class="col-md-6">
							<div class="form-group">
								<label>Job Code</label> <input class="form-control" type="text" name="jobCode"
									placeholder="enter job code" required>
							</div>
							<div class="form-group">
								<label>Job Title</label> <input type="text" name="jobTitle" 
									class="form-control" placeholder="enter job title" required>
							</div>
							<div class="form-group">
								<label>Job Description</label> 
								<textarea  class="form-control" type="text" name="jobDiscription"
									placeholder="enter job discription" required rows="7" cols="3"> </textarea>
							</div>
							<div class="form-group">
								<label>Last Date</label> <input type="text" name="lastDate" id="addjobsCal"
									class="form-control" placeholder="yyyy-mm-dd" required>
							</div>
							
							<div class="form-group">
								<label>No of Post</label> <input class="form-control" name="noofPost"
									placeholder="enter the no of post" required>
							</div>
							<div class="form-group">
								<label>Location</label> <input class="form-control" name="location"
									placeholder="enter the Location" required>
							</div>
							
							<div class="form-group text-center">
								<input type="submit" class="btn btn-md btn-primary" value="Submit"/>
								<input type="reset" class="btn btn-md btn-danger" value="Reset"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.panel-->
	</div>
	<!-- /.col-->


</div>
<!--/.main-->

<%@include file="footer.jsp"%>
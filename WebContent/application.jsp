<%@include file="webResource.jsp"%>
<%@include file="navbar.jsp"%>
<%@include file="leftmenu.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"> <em class="fa fa-home"></em>
			</a></li>
			<li class="active">Dashboard</li>
			<li class="active">Application</li>
		</ol>
	</div>
	<!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">All Application</h1>
		</div>
	</div>
	<!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Data</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Job Code</th>
									<th>Name</th>
									<th>Email</th>
									<th>Age</th>
									<th>City</th>
									<th>Position</th>
									<th>Mobile</th>
									<th>Job Type</th>
									<th>Experience</th>
									<th>Date</th>
									<th>#</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${userList}" var="user">
									<tr>
										<td>${user.jobcode}</td>
										<td>${user.fname}${user.lname}</td>
										<td>${user.email}</td>
										<td>${user.age}</td>
										<td>${user.city}</td>
										<td>${user.position}</td>
										<td>${user.mobile}</td>
										<td>${user.jobtype}</td>
										<td>${user.experience}</td>
										<td>${user.date}</td>
										<td><a
											href="GetUserByIdServlet?userid=${user.userid}&filepath=${user.filepath}"><em
												class="fa fa-file-pdf-o"></em> </a></td>
										<td><a data-toggle="modal" data-target="#sendMailModel"
											onClick="sendmail('${user.userid}', '${user.jobcode}','${user.fname}',' ${user.lname}','${user.email}','${user.position}','${user.mobile}', '${user.filepath}')"><em
												class="fa fa-send"></em></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="sendMailModel" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Send Mail</div>
						<div class="panel-body">
							<form role="form" action="ApplicationServlet" method="post">
								<div class="col-md-12">
									<div class="form-group">
										<input type="hidden" name="userid" id="userid" value=""/>
										<input type="hidden" name="jobcode" id="jobcode" value=""/>
										<input type="hidden" name="name" id="name" value=""/>
										<input type="hidden" name="email" id="email" value=""/>
										<input type="hidden" name="position" id="position" value=""/>
										<input type="hidden" name="mobile" id="mobile" value=""/>
										<input type="hidden" name="filepath" id="filepath" value=""/>
										
										
									</div>
									<div class="form-group">
										<label>To</label> <input class="form-control" type="text"
											value="" name="to" id="to" required>
									</div>
									<div class="form-group">
										<label>Subject</label> <input class="form-control" type="text"
											value="" name="subject" id="subject" required>
									</div>
									
									
								    <div class="form-group">
										<label>Body</label> <textarea rows="5" cols="" class="form-control"
											value="" name="message" id="message" required> </textarea>
									</div>
									
									<label>Applicant Detail</label>
									<div class="d5">
										<div class="d52"><span>Name: </span><span id="nameID"></span></div> 
										<div class="d53"><span>Job Code: </span><span id="codeID"></span></div>
										<div class="">.</div>
									</div>
									
									<div class="form-group text-center">
										<input type="submit" class="btn btn-md btn-primary"
											value="Send mail" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>

<!--/.main-->

<%@include file="footer.jsp"%>
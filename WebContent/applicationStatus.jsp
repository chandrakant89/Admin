<%@include file="webResource.jsp"%>
<%@include file="navbar.jsp"%>
<%@include file="leftmenu.jsp"%>		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
				<li class="active">Application Status</li>
				
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Application Status</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Tables</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Send By</th>
									<th>Applicant Name</th>
									<th>Applied Position</th>
									<th>Send To</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="obj" items="${appstatus}">
									<tr >
										<td>${obj.sendBy }</td>
										<td>${obj.applicantName }</td>
										<td>${obj.appliedPosition }</td>
										<td>${obj.sendTo }</td>
										<td>${obj.date }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	

	</div>	<!--/.main-->
	<%@include file="footer.jsp"%>
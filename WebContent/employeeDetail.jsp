
<%@include file="webResource.jsp"%>
<%@include file="navbar.jsp"%>
<%@include file="leftmenu.jsp"%>		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
				<li class="active">employeeDetail</li>
				
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Employee Detail</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Tables</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Emp Code</th>
									<th>Employee Name</th>
									<th>Date Of Birth</th>
									<th>Gender</th>
									<th>Email Id</th>
									<th>Mobile</th>
									<th>Address</th>
									<th>Designation</th>
									<th>JobType</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="obj" items="${empList}">
									<tr style="cursor: pointer;" onclick="empDetail('${obj.empCode}',
														   '${obj.name}',
														   '${obj.dateOfBirth}',
														   '${obj.gender}',
														   '${obj.mobNo}',
														   '${obj.address}',
														   '${obj.dateOfHire}',
														   '${obj.maritalStatus}',
														   '${obj.dateOfBirth}',
														   '${obj.emailId}',
														   '${obj.empJobType}',
														   '${obj.designation}',
														   '${obj.status}',
														   '${obj.gender}',
														   '${obj.dateOfBirth}',
														   '${obj.gender}',
														   '${obj.emergencyMobNO}',
														   '${obj.panNo}',
														   '${obj.adharNo}',
														   '${obj.filename}')" 
										data-toggle="modal" data-target="#empDetail">
										<td>${obj.empCode }</td>
										<td>${obj.name }</td>
										<td>${obj.dateOfBirth }</td>
										<td>${obj.gender }</td>
										<td>${obj.emailId }</td>
										<td>${obj.mobNo }</td>
										<td>${obj.address }</td>
										<td>${obj.designation }</td>
										<td>${obj.empJobType }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
<div id="empDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading" >Employee Detail <span style="float: right;">Employee Code : <span id="empCode"></span> </span></div>
				<div class="panel-body">
					<div class="col-md-4">
							<div class="form-group">
								<label>Name : </label><span id="name"></span> 
							</div>
							<div class="form-group">
								<label>Date Of Birth : </label><span id="dob"></span> 
							</div>
							<div class="form-group">
								<label>Gender : </label><span id="gen"></span> 
							</div>
							
							<div class="form-group">
								<label>Mobile No : </label><span id="mob"></span> 
							</div>
							<div class="form-group">
								<label>Address : </label><span id="addr"></span> 
							</div>
						
							<div class="form-group">
								<label>Designation : </label><span id="desin"></span> 
							</div>
							
							<div class="form-group">
								<label>Email Id : </label><span id="email"></span> 
							</div>
						
						</div>	
						<div class="col-md-4">
							<div class="form-group">
								<label>Employee Job Type : </label><span id="jobtype"></span> 
							</div>
							
							<div class="form-group">
								<label>Date Of Hire</label><span id="doh"></span> 
							</div>
							
							<div class="form-group">
								<label>Marital Status : </label><span id="mstatus"></span> 
							</div>
							
							<div class="form-group">
								<label>Status : </label><span id="status"></span> 
							</div>
							
							<div class="form-group">
								<label>Emergency Mobile No : </label><span id="emrgyno"></span> 
							</div>
							
							<div class="form-group">
								<label>Pan No : </label><span id="panNo"></span> 
							</div>
				
							<div class="form-group">
								<label>Adhar No : </label><span id="adharNo"></span> 
							</div>
						</div>
						<div class="col-nd-3">
							<div class="form-group text-center">
								<img id="blah" src="image/user.png" class="imgProfile" />
								<h3 class="name"><strong id="empname"></strong></h3>
							</div>
							
						</div>
				</div>
			</div>
		</div>
		<!-- /.panel-->
	</div>
  </div>
</div>

	</div>	<!--/.main-->

	<%@include file="footer.jsp"%>
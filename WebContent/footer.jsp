<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/dcalendar.picker.js"></script>
<script type="text/javascript" src="js/validatation.js"></script>
<script type="text/javascript" src="Appjs/editjobs.js"></script>

 
 
   <script>
	   $('#addjobsCal').dcalendarpicker({
		   format: 'yyyy-mm-dd'
	   });
	  
	   $('#addempCal').dcalendarpicker({
		   format: 'yyyy-mm-dd'
	   });
	   
	   $('#hirempCal').dcalendarpicker({
		   format: 'yyyy-mm-dd'
	   });
	   
	   $('#lastDate').dcalendarpicker({
		   format: 'yyyy-mm-dd'
	   });
	   
	   $('#updatedDate').dcalendarpicker({
		   format: 'yyyy-mm-dd'
	   });
	   var d = new Date();
	   var year = d.getFullYear();
	   document.getElementById("year").innerHTML = year;
	   
	   $(function() {
		    $(window).on("scroll", function() {
		        if($(window).scrollTop() > 100) {
		            $(".header").addClass("active");
		        } else {
		          $(".header").removeClass("active");
		        }
		    });
		});
	   
	   $(document).ready(function(){
		    $('input[type="file"]').change(function(e){
		        var fileName = e.target.files[0].name;
		        document.getElementById("theFile").innerHTML = fileName;
		    });
		});
	   
	   $(document).ready(function(){
		   $('#submit').attr('disabled', 'disabled');
		   $('#checkMeOut').click(function() {
		        if ($(this).is(':checked')) {
		        	$('#submit').removeAttr('disabled');
		        } else {
		            $('#submit').attr('disabled', 'disabled');
		        }
		    });
	   });
	   
  </script>
  
</body>

</html>
<%@include file="webResource.jsp" %>
<c:import url="/jobListServlet" />
<script src='https://www.google.com/recaptcha/api.js'></script>
<nav class="navbar navbar-custom navbar-fixed-top header" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand hidden-xs" href="index.jsp"><span>Nathap </span>Career</a>
				<a class="navbar-brand hidden-lg hidden-sm hidden-md" href="index.jsp"><span><img alt="Nathap" src="image/favicon.png" width="35"></span></a>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
							<a href="login.jsp"  style="background: #30a5ff;color: #fff;">HR Login &nbsp;<em class="fa fa-sign-in"></em></a>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
<div class="page-head">
	<div class="page-caption">
		<div><h2 class="page_head">Get best matched Jobs</h2></div>
		<div class="col-md-4 col-sm-4">
			<a href="uploadcontent.jsp"> 
			<img alt="" src="image/4.jpg" width="100%"></a>
		</div>
		<div class="col-md-4 col-sm-4">
			<a><img alt="" src="image/5.jpg" width="100%">
			</a>
			
		</div>
		<div class="col-md-4 col-sm-4">
			<a><img alt="" src="image/6.jpg" width="100%">
			</a>
			
		</div>
	</div>

</div>
<section>
	<div class="container clearfix">
		<div class="row">
			<div class="col-md-12 main_div">
				<div class="col-md-8 col-sm-7">
					<h2 class="heading">Available Positions</h2>
					<div class="overflow">
						<c:forEach items="${jobList}" var="jlist">
							<div class="jobs_div">
								<h4><span id="jobTitle">${jlist.jobtitle}</span> <span id="jobCode">[ ${jlist.jobcode} ]</span></h4>
								<h5><span><em class="fa fa-map-marker"></em> ${jlist.location} </span> <span class="pull-right">Updated Date - ${jlist.updatedDate}</span></h5>
								<h5><span>No of post - ${jlist.noofpost}</span> <span class="pull-right">Last Date - ${jlist.lastdate}</span></h5>
								<div class="descr">
									<p>${jlist.description}</p>
								</div>
								<div>
									<a class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-lg"
										id="apply" onclick="applyJob('${jlist.jobtitle}','${jlist.jobcode}','${jlist.location}','${jlist.lastdate}','${jlist.updatedDate}','${jlist.noofpost}','${jlist.description}')">See More...</a>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
				<div class="col-md-4 col-sm-5">
					<div class="img_section">
						<a href="uploadcontent.jsp"><img alt="" src="image/2.jpg" width="100%"></a>
						<img alt="" src="image/1.jpg" width="100%">
						<img alt="" src="image/3.jpg" width="100%">
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!-- #content end -->


<!-- popup -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-body">
			<div class="modal-content">
				<div class="modal-body">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
					<div class="jobForm">
						<div class="row">
							<div class="col-md-12 jobs_div_popup">
								<div style="float: left">
									<h4 class=""><span id="jTitle"></span></h4>
								</div>
								<div style="float: right;">
									<h4 class=""><span>JOB CODE :</span> <span id="jobcode1"></span></h4>
								</div>
							</div>
						</div>
						<div class="row">
						<div class="col-md-6">
							<div class="jobs_div_pop">
								<h5><span id="jTitle2"></span> <span class="pull-right" id="jobcode2"></span></h5>
								<h5><em class="fa fa-map-marker"></em> <span id="jlocation"></span></h5>
								<h5>Updated Date - <span id="jupdatedDate"></span></h5>
								<h5>Last Date - <span id="jlast_Date"></span></h5>
								<h5>No of post - <span id="jnoof_posts"></span></h5>
								<div class="details">
									<h4>Job profile</h4>
									<p id="jfulldesc"></p>
								</div>
								<div class="details">
									<h4>Skils</h4>
									<p id="jSkils"></p>
								</div>
								
							</div>
						</div>
						<div class="col-md-6 left_border">
							<form action="jobServlet" id="jobForm" name="jobForm" method="post" enctype="multipart/form-data">
								<input type="hidden" name="jobcode" id="jobcode" value="" class="form-control" />
								<div class="form-group">
									<input placeholder="First Name" type="text" name="fname" id="fname" value="" class="form-control" required maxlength="45" />
									<p class="text-danger" id="fnameError"></p>
								</div>
	
								<div class="form-group">
									<input placeholder="Last Name" type="text" name="lname" id="lname" value="" class="form-control" required maxlength="45" />
									<p class="text-danger" id="lnameError"></p>
								</div>
	
								<div class="form-group">
									<input placeholder="Email" type="email" id="email" name="email" value="" class="email form-control" required maxlength="45" />
									<p class="text-danger" id="emailError"></p>
								</div>
	
								<div class="form-group">
									<input placeholder="Age" type="text" name="age" id="age" value="" onkeypress="javascript:return isNumber(event)" maxlength="2"
									 class="form-control" required min="18" max="50"/>
									<p class="text-danger" id="ageError"></p>
								</div>
	
								<div class="form-group">
									<input placeholder="City" type="text" name="city" id="city" value="" class="form-control" required maxlength="45" />
									<p class="text-danger" id="cityError"></p>
								</div>
	
								<div class="form-group">
									<input placeholder="Position" type="text" name="position" id="position" value="" class="form-control" required readonly="readonly" />
								</div>
	
								<div class="form-group">
									<input placeholder="Mobile No" type="text" id="mob" name="mob" onkeypress="javascript:return isNumberMob(event)" maxlength="10" value=""
									 class="form-control" required />
									<p class="text-danger" id="mobError"></p>
								</div>
	
								<div class="form-group">
									<select name="jobtype" id="jobtype" class="form-control" required>
										<option value="">--select Job Type --</option>
										<option value="fullTime">Full Time</option>
										<option value="partTime">Part Time</option>
									</select>
									<p class="text-danger" id="jobtError"></p>
								</div>
	
								<div class="form-group">
									<select name="experience" id="experience" class="form-control" required>
										<option value="">--select experience --</option>
										<option value="0">0 Year </option>
										<option value="<1">
											< 1 Year </option> <option value="1"> 1 Year
										</option>
										<option value="2"> 2 Year </option>
										<option value="3"> 3 Year </option>
										<option value="4"> 4 Year </option>
										<option value="5"> 5 Year </option>
										<option value=">5"> > 5 Year </option>
									</select>
									<p class="text-danger" id="exepiError"></p>
								</div>
	
								<div class="form-group">
									<textarea placeholder="Remark..." name="application" id="application" rows="6" maxlength="250" class="form-control" required></textarea>
									<p class="text-danger" id="remarkError"></p>
								</div>
	
								<div class="form-group">
									<label for="template-jobform-application">Upload Resume <small>*</small></label>
									<input type="file" name="file" id="file" required style="box-shadow: 5px 7px 7px 1px transparent !important; border: 1px solid transparent !important;" />
									<p class="text-danger" id="fileError"></p>
									<p>Supported Formats: doc, docx, pdf, upto 10 MB </p>
								</div>
	
								<!-- For localhost site key -->
	
								<!-- <div class="form-group">
									<div class="g-recaptcha" data-sitekey="6LfC_lwUAAAAAPQOKDIuX57NGA3S0UXTJrE9LnU2" data-callback="YourOnSubmitFn"></div>
									<p class="text-danger" id="captcha"></p>
								</div> -->
	
								<!-- for live project -->
								<div class="form-group">
									<div class="g-recaptcha" data-sitekey="6LdJjI4UAAAAAAb5dpznIbNqCzfFiEetmTyD7DEj" data-callback="YourOnSubmitFn"></div>
									<p class="text-danger" id="captcha"></p>
								</div>
	
	
								<div class="form-group text-center">
									<input class="btn btn-primary" id="submit" type="submit" value="Send Application" onclick="return ValidateExtensionApplyJob()" />
								</div>
							</form>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<footer>
<div class="container">
    <p><a>Copyrights <span id="year"></span> All Rights Reserved by Nathap.</a></p>
  </div>
</footer>
<%@include file="footer.jsp" %>
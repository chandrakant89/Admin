<%@include file="webResource.jsp"%>
<%@include file="navbar.jsp"%>
<%@include file="leftmenu.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"> <em class="fa fa-home"></em>
			</a></li>
			<li class="active">Dashboard</li>
			<li class="active">JobsDetail</li>

		</ol>
	</div>
	<!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Jobs Details</h1>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Tables</div>
				<div class="panel-body">
					<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Job Code</th>
								<th>Job Title</th>
								<th>Description</th>
								<th>Last Date</th>
								<th>Post</th>
								<th>#</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="obj" items="${jobsList}">
								<tr>
									<td>${obj.jobcode }</td>
									<td>${obj.jobtitle }</td>
									<td style="width: 400px"><p class="elips">${obj.description }</p></td>
									<td>${obj.lastdate }</td>
									<td>${obj.noofpost }</td>
									<td>
										<a data-toggle="modal" data-target="#editModal" class="btn " onclick="editJobs('${obj.jobcode}','${obj.jobtitle}','${obj.description}','${obj.lastdate}','${obj.noofpost}','${obj.status}')"><em class="fa fa-pencil"></em></a>
									</td>
									<td>${obj.status }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Edit job Detail</div>
				<div class="panel-body">
					<form role="form" action="EditJobsServlet" method="post">
						<div class="col-md-12">
							<div class="form-group">
								<label>Job Code</label> <input class="form-control" type="text" value="" 
								name="jobCode" id="jobCode" readonly required>
							</div>
							<div class="form-group">
								<label>Job Title</label> <input type="text" name="jobTitle" id="jobTitle" value=""  
									class="form-control" readonly required>
							</div>
							<div class="form-group">
								<label>Job Description</label> 
								<textarea class="form-control" type="text" value=""  rows="7"
								name="jobDiscription" id="jobDiscription" required></textarea>
							</div>
							<div class="form-group">
								<label>Last Date</label> <input type="text" name="lastDate" id="lastDate" value="" 
									class="form-control" required>
							</div>
							<div class="form-group">
								<label>No of Post</label> <input class="form-control" name="noofPost" id="noofPost" value="" required>
							</div>
							
							<div class="form-group">
								<label>Update Status</label> 
								<select class="form-control" name="status" id="status" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
							
							<div class="form-group text-center">
								<input type="submit" class="btn btn-md btn-primary" value="Submit"/>
								<input type="reset" class="btn btn-md btn-danger" value="Reset"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.panel-->
	</div>
  </div>
</div>
<script type="text/javascript" src="Appjs/editjobs.js"></script>
	<!--/.main-->
	<%@include file="footer.jsp"%>
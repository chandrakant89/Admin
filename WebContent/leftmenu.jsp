<%
	String user3= null;
	Object sess = session.getAttribute("username");
	if(sess == null){
		response.sendRedirect("index.jsp");
	}else{
	 user3	= session.getAttribute("username").toString();
	}
%>
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="https://scontent.fdel8-1.fna.fbcdn.net/v/t1.0-9/12801582_835229126604833_2795740960873867336_n.jpg?_nc_cat=0&oh=1bc7519c6f57a296489ed82a4bbdb0e6&oe=5B9E0004&efg=eyJhZG1pc3Npb25fY29udHJvbCI6MCwidXBsb2FkZXJfaWQiOiIxMDAwMDM1MzI1OTU1OTQifQ%3D%3D" width="30" height="30" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><%= user3 %></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class="active" id="dashboardId"><a href="dashboardServlet"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="" id="addJobsId"><a href="addJobs.jsp"><em class="fa fa-calendar">&nbsp;</em> Add Jobs</a></li>
			<li class="" id="jobDetailId"><a href="JobsServlet"><em class="fa fa-file">&nbsp;</em> Jobs Detail</a></li>
			<li class="" id="allappID"><a href="ApplicationServlet"><em class="fa fa-folder">&nbsp;</em> All Application</a></li>
			<li class="" id="addEmpId"><a href="addEmployee.jsp"><em class="fa fa-id-badge">&nbsp;</em> Add Employee</a></li>
			<li class="" id="empdetaId"><a href="EmpDetailServlet"><em class="fa fa-group">&nbsp;</em> Employee Detail</a></li>
			<li class="" id="applStatusId"><a href="applicationStatus.jsp"><em class="fa fa-code-fork">&nbsp;</em> Application Status</a></li>
			<!-- <li><a href="skills.jsp"><em class="fa fa-cubes">&nbsp;</em> Skills</a></li> -->
			
		</ul>

	</div><!--/.sidebar-->
<%@include file="webResource.jsp"%>
	<div class="">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		<div class="text-center"><a href="https://nathap.in"><img alt="" src="image/logo.png"> </a>	</div>
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form role="form" action="LoginServlet" method="post" style="padding: 20px;">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="please enter user name or e-mail id" name="userName" type="text" autofocus="" required>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="please enter your password" name="password" type="password" value="" required>
							</div>
							<div class="form-group ${err}"><span class="text-danger">${error}</span></div>
							<div class="form-group">
								<input type="submit" class=" col-xs-4 col-xs-offset-4 btn btn-md btn-primary" value="Login"/>
							</div>
							
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
	<%@include file="footer.jsp"%>
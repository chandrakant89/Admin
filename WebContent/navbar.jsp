<%
	String user= null;
	Object ss = session.getAttribute("username");
	if(ss == null){
		response.sendRedirect("index.jsp");
	}else{
	 user	= session.getAttribute("username").toString();
	}
%>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand hidden-xs" href="dashboardServlet"><span>Nathap</span>Admin</a>
				<a class="navbar-brand hidden-lg hidden-sm hidden-md" href="dashboardServlet"><span><img alt="Nathap" src="image/favicon.png" width="35"></span></a>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-user"></em> <span><%=user %></span> 
					</a>
						<ul class="dropdown-menu dropdown-alerts">
							<li><a href="logout.jsp"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	
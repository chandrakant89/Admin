<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@include file="webResource.jsp" %>
<c:import url="/jobListServlet" />
<script src='https://www.google.com/recaptcha/api.js'></script>
<nav class="navbar navbar-custom navbar-fixed-top header" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand hidden-xs" href="index.jsp"><span>Nathap </span>Career</a>
				<a class="navbar-brand hidden-lg hidden-sm hidden-md" href="index.jsp"><span><img alt="Nathap" src="image/favicon.png" width="35"></span></a>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
							<a href="login.jsp"  style="background: #30a5ff;color: #fff;">HR Login &nbsp;<em class="fa fa-sign-in"></em></a>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
 <div class="page-head">
     <div class="container">
         <div class="row">
             <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                 <div class="page-caption">
                     <h1 class="page-title">Get best matched Jobs</h1>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <section>
 	<div class="container clearfix content_body">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<h4 class="heading_success"> ${message} </h4>
				
				<div class="text-center">
					<a href="index.jsp" class="btn btn-primary">Back to Home Page</a>
				</div>
				
			</div>
		</div>
	</div>
 </section>
<footer>
  <div class="container">
    <p><a>Copyrights <span id="year"></span> All Rights Reserved by Nathap.</a></p>
  </div>
</footer>
<%@include file="footer.jsp" %>
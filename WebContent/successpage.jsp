<%@include file="webResource.jsp"%>
<%@include file="navbar.jsp"%>
<%@include file="leftmenu.jsp"%>		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
				<li class="active">Success Page</li>
				
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Success</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="message_div">
					<h4 class="heading_success">${message}</h4>
				</div>
				
			</div>
		</div>
	

	</div>	<!--/.main-->
	<%@include file="footer.jsp"%>
<%@include file="webResource.jsp"%>
<nav class="navbar navbar-custom navbar-fixed-top header"
	role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#sidebar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand hidden-xs" href="index.jsp"><span>Nathap
			</span>Career</a> <a class="navbar-brand hidden-lg hidden-sm hidden-md"
				href="index.jsp"><span><img alt="Nathap"
					src="image/favicon.png" width="35"></span></a>
			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown"><a href="login.jsp"
					style="background: #30a5ff; color: #fff;">HR Login &nbsp;<em
						class="fa fa-sign-in"></em></a></li>
			</ul>
		</div>
	</div>
	<!-- /.container-fluid -->
</nav>
<div class="page-head">
	<div class="container">
		<div class="row">
			<div class="panel-body_0">
				<div><h2 class="page-title">Upload Your Content</h2></div>
			</div>
		</div>
	</div>
</div>
<div class="section_0">
	<div class="container">
		<div class="row">
			<div><h3 class="page-title0">First let's know what is social khichdi ?</h3>
				<div class="content">
					<p>It is known by the name of social network as well - it is literature, science, sports, politics, cinema, society. This portal provides readable, entertaining, and informative content available to all these dimensions, available at one place. </p>
				</div>
			</div>
			
			<div><h3 class="page-title0">What to do</h3>
				<div class="content">
					<p>If you are passionate about writing and have a sharp edge in your writing, then social discourse is for you. Share a sample of your writing that is printed and printed from anywhere else. If we liked your writing and fulfilled our standards, then your writing will be published on your name and picture on social media. We do not mean by your degree / merit. Just writing should be strong and effective. 
					If you are studying now, you can join us as an intern, if you have begun studying, then your work will be tested for 6 months. After that, if your line length is correct then your Sixteen socialists will get accustomed.</p>
				</div>
			</div>
			
		</div>
	</div>
</div>
<div class="section_1">
	<div class="container">
		<div class="row">
				<div class="panel-body page-caption_content">
					<div><h3 class="page-title1">Upload Your Content</h3></div>
					<form role="form" action="writerDetailServlet" method="post"
						enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group col-md-6 cont">
								<label>Name</label> <input class="form-control" type="text"
									name="name" placeholder="Enter Name" maxlength="20" required>
							</div>
							<div class="form-group col-md-6 cont">
								<label>Mobile</label> <input type="text" name="mobile" maxlength="10"
									class="form-control" placeholder="Enter Mobile" required>
							</div>
							<div class="form-group col-md-6 cont">
								<label>Qualification</label> <input type="text" name="qualification"
									class="form-control" placeholder="Enter Qualification" required maxlength="25">
							</div>
							
							<div class="form-group col-md-6 cont">
								<label>Email</label> <input class="form-control" name="email" type="email"
									placeholder="Enter the Email" required>
							</div>

							<div class="form-group col-md-6 cont">
								<label>Address</label> <input class="form-control"
									name="location" placeholder="Enter the Location" required maxlength="45">
							</div>
							
							<div class="form-group col-md-6 cont">
								<label>URL ( if you have any video)</label> <input class="form-control"
									name="url" placeholder="Enter the url" maxlength="100">
							</div>
							
							<div class="form-group col-md-6 cont">
								<label>Bio</label> 
								<textarea  name="bio" cols="3" rows="5" maxlength="200"
									class="form-control" placeholder="Enter bio" required></textarea>
							</div>

							<div class="form-group col-md-12 cont ">
								<div class="upload-btn-wrapper">
								 	<button class="btn_upload">Upload Content</button>
									<input type="file" name="file" required>
								</div>
								<label id="theFile"></label>
								<div class="text-success">Supported Formats: doc, docx, pdf, upto 10 MB</div>
							</div>
							
							<div class="form-group col-md-12 cont">
								<label class="contt">
								  <input type="checkbox" name="termsconditions"  id="checkMeOut" value="YES" required="required">
								  <span class="checkmark"></span>
								</label>
								<div class="term">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
							</div>

							<div class="form-group text-center col-md-12">
								<input type="submit" class="btn btn-md btn-primary" id="submit"
									value="Submit" />
								<input type="reset"
									class="btn btn-md btn-danger" value="Reset" />
								<a href="index.jsp" class="btn btn-md btn-danger">Back</a>
							</div>
						</div>
					</form>
				</div>
		</div>
	</div>
</div>
<footer>
	<div class="container">
		<p>
			<a>Copyrights <span id="year"></span> All Rights Reserved by
				Nathap.
			</a>
		</p>
	</div>
</footer>
<%@include file="footer.jsp"%>
package com.admin.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin.dao.ApplicationStatusDao;
import com.admin.dao.UserDao;
import com.admin.dto.ApplicationStatusDto;
import com.admin.dto.UserDetailDto;
import com.admin.mailer.Mailer;

public class ApplicationServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		if(user1 != null){
		List<UserDetailDto> user = null;
		UserDao userDao = new UserDao();
		user = userDao.getUserList();
		req.setAttribute("userList",user);
		req.getRequestDispatcher("/application.jsp").forward(req, res);
		}else{
			req.getRequestDispatcher("/logout.jsp").forward(req, res);
		}
	}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		System.out.println("Inside send mail and save application status");
		System.out.println("User: "+user1);
		if(user1 != null){
		
		String to = req.getParameter("to");
		String bcc = "hr@nathap.in";
		String subject = req.getParameter("subject");
		String message = req.getParameter("message");
		String jobcode = req.getParameter("jobcode");
		String applicantName = req.getParameter("name");
		String email = req.getParameter("email");
		String appliedPosition = req.getParameter("position");
		String mobile = req.getParameter("mobile");
		String filepath = req.getParameter("filepath");
		
		
		String user2 = "Vishnu Kant"; 
		
		System.out.println(jobcode+" "+applicantName+" "+email+" "+appliedPosition+" "+mobile+" "+filepath);
		
		String t = "The content of this message is confidential.If you have received it by mistake, please inform us by an email reply and then delete the message. It is forbidden to copy, forward, or in any way reveal the contents of this message to anyone. The integrity and security of this email cannot be guaranteed over the Internet. Therefore, the sender will not be held liable for any damage caused by the message.";
		String body = "<html><head><style type="+"text/css"+">"
				+".d{ font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; margin: 0 auto; max-width: 650px; color: #555;}"
				+".d1{height: 20px;}"
				+".d2{padding: 20px;}"
				+".d2 .img{text-align: center; border-bottom: 1px solid #555; padding: 10px;}"
				+".d3{text-align: center; padding: 30px; font-size: 18px;}"
				+".d4{padding: 10px 20px; font-size: 11px;}"
				+".d5{padding: 10px 0px 50px 0px;  border-bottom: 1px solid #ccc;}"
				+".d5 p span:nth-of-type(1){font-weight: 600;}"
				+"p{font-size: 13px;}"
				+"</style></head><body>"
				+"<div class="+"d"+">"
				+"<div class="+"d2"+"><div class="+"img"+"><a href="+"https://nathap.in/"+"><img src="+"https://nathap.in/images/logo.png"+"></a></div>"
				+"<div class="+"d3"+">Applicant Details</div>"
				+"<p>Hi "+user2+"</p>"
				+"<p>"+message+"</p>"
				+"<div class="+"d5"+">"
				+"<p><span>Name: </span> <span>"+applicantName+"</span> </p>"
				+"<p><span>Job Code: </span> <span>"+jobcode+"</span> </p>"
				+"<p><span>Email Id: </span> <span>"+email+"</span> </p>"
				+"<p><span>Applied Position: </span><span>"+appliedPosition+"</span> </p>"
				+"<p><span>Mobile No: </span> <span>"+mobile+"</span> </p>"
				+"</div></div><div class="+"d4"+">"
				+"<div>Copyrights � 2018 All Rights Reserved.</div>"
				+"<div>You subscribeed to our newsletter via our website, nathap.in</div>"
				+"</div><div class="+"d4"+">"+t+"</div></div></body></html>";
		
		ApplicationStatusDto applicationStatusDto = new ApplicationStatusDto();
		applicationStatusDto.setSendBy(user1);
		applicationStatusDto.setSendTo(user2);
		
		applicationStatusDto.setApplicantName(applicantName);
		applicationStatusDto.setAppliedPosition(appliedPosition);
		
		ApplicationStatusDao apliApplicationStatusDao = new ApplicationStatusDao();
		boolean saveStatus = apliApplicationStatusDao.saveApplicationStatus(applicationStatusDto);
		
			if (saveStatus) {
				System.out.println("Application Status Saved Successfully");
				// String attachfile = "D:/files/1111362670hello.docx";
				String attachfile = filepath;
				if (to != null && to.length() > 0) {
					Mailer.sendMail(to, bcc, subject, body, attachfile);
				} else {
					System.out.println("Please Fill The Email Id");
				}
				req.setAttribute("message","Mail has been send to " + to);
				req.getRequestDispatcher("successpage.jsp").forward(req, res);
			} else {
				req.setAttribute("message","Application Status Not Saved and mail not send");
				req.getRequestDispatcher("errorpage.jsp").forward(req, res);
			}
		}else{
			req.getRequestDispatcher("/logout.jsp").forward(req, res);
		}
    }

}


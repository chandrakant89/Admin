package com.admin.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin.dao.ApplicationStatusDao;
import com.admin.dto.ApplicationStatusDto;


@SuppressWarnings("serial")
public class ApplicationStatusServlet extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		System.out.println("ApplicationStatusServlet");
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		if(user1 != null){
		List<ApplicationStatusDto> data = null;
		ApplicationStatusDao applicationStatusDao = new ApplicationStatusDao();
		data = applicationStatusDao.getApplicationStatusList();
				
		req.setAttribute("appstatus",data);
		req.getRequestDispatcher("/applicationStatus.jsp").forward(req, res);
		}else{
			req.getRequestDispatcher("/logout.jsp").forward(req, res);
		}
	
	}
}

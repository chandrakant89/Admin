package com.admin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class AutoMailServlet {
	private int totalMails;
	private String data = "chandra@nathap.in, vishnu@gmail.com, arpit@nathap.in, hariom@nathap.in";
	private List<String> mails;
	public AutoMailServlet() {
		mails = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(data, ",");
		
		while(st.hasMoreTokens()) {
			mails.add(st.nextToken().trim());
		}
		totalMails = mails.size();
	}
	
	public List<String> getData(String query) {
		String mail = null;
		query = query.toLowerCase();
		List<String> matched = new ArrayList<String>();
		for(int i=0; i<totalMails; i++) {
			mail = mails.get(i).toLowerCase();
			if(mail.startsWith(query)) {
				matched.add(mails.get(i));
			}
		}
		return matched;
	}
}

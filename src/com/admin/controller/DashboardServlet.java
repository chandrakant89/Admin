package com.admin.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin.dao.DashboardDao;

public class DashboardServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		if(user1 != null){
		DashboardDao dsd = new DashboardDao();
		int tot = dsd.getTotalJobs();
		int total = dsd.getTotalApplication();
		int alluser = dsd.getTotalUser();
		req.setAttribute("totalJobs", tot);
		req.setAttribute("totalApplication", total);
		req.setAttribute("totaluser", alluser);
		
		req.getRequestDispatcher("/dashboard.jsp").forward(req, res);
		}else{
			req.getRequestDispatcher("/logout.jsp").forward(req, res);
		}
		
	}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		
    }

}

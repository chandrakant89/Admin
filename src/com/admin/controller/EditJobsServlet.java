package com.admin.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin.dao.JobsPostDao;
import com.admin.dto.JobsDto;
import com.admin.util.Helper;

public class EditJobsServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		boolean jobpost = false;
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		
		if(user1 != null){
		
		JobsPostDao jobDAO = new JobsPostDao();
		
		String jobscode = req.getParameter("jobCode");
		String jobtitle = req.getParameter("jobTitle");
		String jobDiscript = req.getParameter("jobDiscription");
		String lastdate =  req.getParameter("lastDate");
		String noofpost = req.getParameter("noofPost");
		int status = Integer.parseInt(req.getParameter("status"));

		JobsDto jobsDto = new JobsDto();
		
		jobsDto.setJobcode(jobscode);
		jobsDto.setJobtitle(jobtitle);
		jobsDto.setDescription(jobDiscript);
		jobsDto.setLastdate(lastdate);
		jobsDto.setUpdatedDate(Helper.getCurrentDate());
		jobsDto.setNoofpost(noofpost);
		jobsDto.setStatus(status);

		jobpost = jobDAO.editJob(jobsDto);
		
				if (jobpost) {
					req.setAttribute("message","Jobs Post has been successfully submitted.");
					req.setAttribute("cls", "style-msg successmsg text-center");
					req.setAttribute("sbmsg", "sb-msg");
					req.getRequestDispatcher("/successpage.jsp").forward(req, res);
				} else {
					req.setAttribute("message", "Opps...... Something is wrong");
					req.setAttribute("cls", "style-msg errormsg text-center");
					req.setAttribute("sbmsg", "sb-msg");
					req.getRequestDispatcher("/errorpage.jsp").forward(req, res);
				}
		}else{
			req.getRequestDispatcher("/logout.jsp").forward(req, res);
		}
	} 

}

package com.admin.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin.dao.EmployeeDao;
import com.admin.dto.EmployeeDto;

public class EmpDetailServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		if(user1 != null){
		List<EmployeeDto> data = null;
		EmployeeDao employeeDao = new EmployeeDao();
		data = employeeDao.getEmployeeList();
				
		req.setAttribute("empList",data);
		req.getRequestDispatcher("/employeeDetail.jsp").forward(req, res);
		}else{
			req.getRequestDispatcher("/logout.jsp").forward(req, res);
		}
	}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {}

}

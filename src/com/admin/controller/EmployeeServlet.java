package com.admin.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.admin.dao.EmployeeDao;
import com.admin.dto.EmployeeDto;

@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)	// 50MB
public class EmployeeServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		
		if(user1 != null){
			req.getRequestDispatcher("/addEmployee.jsp").forward(req, res);	
		}else{
			req.getRequestDispatcher("/logout.jsp").forward(req, res);
		}
		
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		
		if(user1 != null){
		boolean saveemp= false;
		
		 //String empCode = req.getParameter("empCode");
		 String name = req.getParameter("name");
		 String dateOfBirth = req.getParameter("dateOfBirth");
		 String gender = req.getParameter("gender");
		 String maritalStatus = req.getParameter("maritalStatus");
		 String mobNo = req.getParameter("mobNo");
		 String address = req.getParameter("address");
		 String dateOfHire = req.getParameter("dateOfHire");
		 String emailId = req.getParameter("emailId");
		 String empJobType = req.getParameter("empJobType");
		 String designation = req.getParameter("designation");
		 String status = req.getParameter("status");
		 String emergencyMobNO = req.getParameter("emergencyMobNO");
		 String panNo = req.getParameter("panNo");
		 String adharNo = req.getParameter("adharNo");
		 Part part = req.getPart("photo");
		 String fileName=extractFileName(part);
		 
		 EmployeeDto employeeDto = new EmployeeDto();
		 
		 employeeDto.setName(name);
		 employeeDto.setDateOfBirth(dateOfBirth);
		 employeeDto.setGender(gender);
		 employeeDto.setMaritalStatus(maritalStatus);
		 employeeDto.setMobNo(mobNo);
		 employeeDto.setAddress(address);
		 employeeDto.setDateOfHire(dateOfHire);
		 employeeDto.setEmailId(emailId);
		 employeeDto.setEmpJobType(empJobType);
		 employeeDto.setDesignation(designation);
		 employeeDto.setStatus(status);
		 employeeDto.setEmergencyMobNO(emergencyMobNO);
		 employeeDto.setPanNo(panNo);
		 employeeDto.setAdharNo(adharNo);
		 employeeDto.setPhoto(part);
		 employeeDto.setFilename(fileName);
		 
		 EmployeeDao employeeDao = new EmployeeDao();
		 saveemp = employeeDao.addEmployee(employeeDto);
		 
		 if(saveemp){
			 	req.setAttribute("message","Employee Datail has been successfully Saved.");
				req.setAttribute("cls", "style-msg successmsg text-center");
				req.setAttribute("sbmsg", "sb-msg");
				req.getRequestDispatcher("/successpage.jsp").forward(req, res);
		 	}else{
		 		req.setAttribute("message", "Opps...... Something is wrong");
				req.setAttribute("cls", "style-msg errormsg text-center");
				req.setAttribute("sbmsg", "sb-msg");
				req.getRequestDispatcher("/errorpage.jsp").forward(req, res);
		 	}
		 
		} else{
			req.getRequestDispatcher("/login.jsp").forward(req, res);
		} 
		 
	}
	
	private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }

}


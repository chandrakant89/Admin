package com.admin.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.admin.dao.UserDao;
import com.admin.dto.UserDetailDto;



public class GetUserByIdServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		int userid = Integer.parseInt(req.getParameter("userid"));
		String userfilepath = req.getParameter("filepath");
		UserDao userDao = new UserDao();
		UserDetailDto userDetailDto =  new UserDetailDto();
		userDetailDto = userDao.getUserById(userid, userfilepath);
		
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		
		//String path = "D:\\files\\2671079108shashi kant _resume.doc";
		String path = userDetailDto.getFilepath();
		File f = new File(path);
		if(!f.exists()){
			System.out.println("File does't exits on server");
			//res.sendRedirect("ApplicationServlet.servlet");
			
		}else{
			
			String filename = f.getName();
			String filepath = "D://files//";
			//String filepath = "/home/thenatur//files//";
			res.setContentType("APPLICATION/OCTET-STREAM");
			res.setHeader("Content-Disposition", "attachment; filename=" + filename + "");
			FileInputStream fileInputStream = new FileInputStream(filepath + filename);
					int i;
					while ((i = fileInputStream.read()) != -1) {
						out.write(i);
					}
					fileInputStream.close();
					out.close();
			
			}
		
		}

}

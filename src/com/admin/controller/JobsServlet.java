package com.admin.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin.dao.JobsDao;
import com.admin.dto.JobsDto;

public class JobsServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		HttpSession s = req.getSession(true);
		String user1 = (String) s.getAttribute("username");
		
		if(user1 != null){
		List<JobsDto> jobsDto = null;
		JobsDao jobsDao = new JobsDao();
		jobsDto = jobsDao.getjobsList();

		req.setAttribute("jobsList",jobsDto);
		req.getRequestDispatcher("/jobsDetail.jsp").forward(req, res);
		}else{
			req.getRequestDispatcher("/logout.jsp").forward(req, res);
		}
	}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		
    }

}

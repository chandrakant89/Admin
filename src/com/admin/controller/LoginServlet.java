package com.admin.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin.dao.DashboardDao;
import com.admin.dao.LoginDao;
import com.admin.dto.LoginDto;

public class LoginServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		
		HttpSession session=req.getSession(); 
		LoginDto userValid = null;
		
		String username = req.getParameter("userName");
		String password = req.getParameter("password");
		
		LoginDto loginDto =  new LoginDto();
		loginDto.setUserName(username);
		loginDto.setPassword(password);
		
		userValid = LoginDao.isValidate(loginDto);
	
		if(userValid.getUserName() != null){
			session.setAttribute("username",userValid.getUserName());
			DashboardDao dsd = new DashboardDao();
			int tot = dsd.getTotalJobs();
			int total = dsd.getTotalApplication();
			int alluser = dsd.getTotalUser();
			req.setAttribute("totalJobs", tot);
			req.setAttribute("totalApplication", total);
			req.setAttribute("totaluser", alluser);
			
			req.getRequestDispatcher("dashboard.jsp").forward(req, res);
		}else{
			req.setAttribute("err", "error");
			req.setAttribute("error", "username or password invalid");
			req.getRequestDispatcher("/login.jsp").forward(req, res);
		}
		
		
		
    }

}

package com.admin.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.admin.dto.ApplicationStatusDto;
import com.admin.util.DBConnection;

public class ApplicationStatusDao {
public boolean saveApplicationStatus(ApplicationStatusDto applicationStatusDto) throws IOException {
		
		boolean flag = false;
		Date d = new Date();
		SimpleDateFormat dateformate = new SimpleDateFormat("yyyy-MM-dd");
		
		String sendBy = applicationStatusDto.getSendBy();
		String applicantName = applicationStatusDto.getApplicantName();
		String appliedPosition = applicationStatusDto.getAppliedPosition();
		String sendTo = applicationStatusDto.getSendTo();
		String date = dateformate.format(d);
		
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		 try {
			 con = DBConnection.createConnection();
			 String query = "insert into applicationstatus(id,sendBy, applicantName, appliedPosition, sendTo, date) "
			 		+ "values (NULL,?,?,?,?,?)";
			 preparedStatement = con.prepareStatement(query);
			 preparedStatement.setString(1, sendBy);
			 preparedStatement.setString(2, applicantName);
			 preparedStatement.setString(3, appliedPosition);
			 preparedStatement.setString(4, sendTo);
			 preparedStatement.setString(5, date);
			 
			 int i= preparedStatement.executeUpdate();
			 if (i!=0) {
				 flag = true;
				 return flag;
			 }else {
				 flag = false;
			 }
		 }catch(SQLException e) {
			 e.printStackTrace();
		 }catch(NullPointerException e) {
			 e.printStackTrace();
		 }
		finally {

			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		 return flag;
		
	}


public List<ApplicationStatusDto> getApplicationStatusList() {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		List<ApplicationStatusDto> appstatuslist = new ArrayList<>();

		try {
			con = DBConnection.createConnection();
			ps = con.prepareStatement("select * from applicationstatus");
			rs = ps.executeQuery();
			while (rs.next()) {
				ApplicationStatusDto applicationStatusDto = new ApplicationStatusDto();
				applicationStatusDto.setSendBy(rs.getString("sendBy"));
				applicationStatusDto.setApplicantName(rs.getString("applicantName"));
				applicationStatusDto.setAppliedPosition(rs.getString("appliedPosition"));
				applicationStatusDto.setSendTo(rs.getString("sendTo"));
				applicationStatusDto.setDate(rs.getString("date"));
				
				appstatuslist.add(applicationStatusDto);
			}

		} catch (Exception e) {
			System.out.println(e);
			appstatuslist = new ArrayList<>();
		} finally {
			try {
				if (con != null && ps != null) {
					con.close();
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return appstatuslist;
	}
}

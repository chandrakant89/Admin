package com.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.admin.util.DBConnection;

public class DashboardDao {

	public int getTotalJobs() {
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int data = 0;
		try {
			 con = DBConnection.createConnection();
			 String sql ="select count(*) total from jobdetail";
			 preparedStatement = con.prepareStatement(sql);
			 resultSet = preparedStatement.executeQuery();			 
			 while(resultSet.next()){
				  data = resultSet.getInt("total");
			 }
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		return data;
	}
	
	public int getTotalApplication() {
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int data = 0;
		try {
			 con = DBConnection.createConnection();
			 String sql ="select count(*) total from userdetail";
			 preparedStatement = con.prepareStatement(sql);
			 resultSet = preparedStatement.executeQuery();			 
			 while(resultSet.next()){
				  data = resultSet.getInt("total");
			 }
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		return data;
	}

	public int getTotalUser() {
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int data = 0;
		try {
			 con = DBConnection.createConnection();
			 String sql ="select count(*) total from logindetail";
			 preparedStatement = con.prepareStatement(sql);
			 resultSet = preparedStatement.executeQuery();			 
			 while(resultSet.next()){
				  data = resultSet.getInt("total");
			 }
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}	
		return data;
	}

}
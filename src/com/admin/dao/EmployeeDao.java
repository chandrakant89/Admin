package com.admin.dao;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.Part;

import com.admin.dto.EmployeeDto;
import com.admin.util.DBConnection;

public class EmployeeDao {
	private static final String SAVE_DIR="photos";
	public boolean addEmployee(EmployeeDto employeeDto) throws IOException {

		boolean flag = false;
		String savePath = "D:/" + SAVE_DIR;
		//String savePath = "/home/thenatur//employee/" + SAVE_DIR;
		//String savePath = "/home/thenatur//public_html/awebbit.com/NATHAPIMG/";
		
		File fileSaveDir=new File(savePath);
        if(!fileSaveDir.exists()){
            fileSaveDir.mkdir();
        }
		
		//String  empCode = employeeDto.getEmpCode();
		String name = employeeDto.getName();
		String dateOfBirth = employeeDto.getDateOfBirth();
		String gender = employeeDto.getGender();
		String maritalStatus = employeeDto.getMaritalStatus();
		String mobNo = employeeDto.getMobNo();
		String address = employeeDto.getAddress();
		String dateOfHire = employeeDto.getDateOfHire();
		String emailId = employeeDto.getEmailId();
		String empJobType = employeeDto.getEmpJobType();
		String designation = employeeDto.getDesignation();
		String status = employeeDto.getStatus();
		String emergencyMobNO = employeeDto.getEmergencyMobNO();
		String panNo = employeeDto.getPanNo();
		String adharNo = employeeDto.getAdharNo();
		Part part = employeeDto.getPhoto();
		String filename = employeeDto.getFilename();
		
		long num = generateRandNumber();
		part.write(savePath + "/"+ num + filename);
		
		

		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			con = DBConnection.createConnection();
			String query = "insert into employeedetail(empCode,name, dateOfBirth, gender, maritalStatus, mobNo, address, dateOfHire, emailId, empJobType, designation, status, emergencyMobNO, panNo, adharNo, photo) values (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			preparedStatement = con.prepareStatement(query);
			//preparedStatement.setString(1, empCode);
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, dateOfBirth);
			preparedStatement.setString(3, gender);
			preparedStatement.setString(4, maritalStatus);
			preparedStatement.setString(5, mobNo);
			preparedStatement.setString(6, address);
			preparedStatement.setString(7, dateOfHire);
			preparedStatement.setString(8, emailId);
			preparedStatement.setString(9, empJobType);
			preparedStatement.setString(10, designation);
			preparedStatement.setString(11, status);
			preparedStatement.setString(12, emergencyMobNO);
			preparedStatement.setString(13, panNo);
			preparedStatement.setString(14, adharNo);
			String filepath= savePath + "/" + num + filename ;
			preparedStatement.setString(15, filepath);
			

			int i = preparedStatement.executeUpdate();
			if (i != 0) {
				flag = true;
				return flag;
			} else {
				flag = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} finally {

			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return flag;
	}
	
	public static long generateRandNumber(){
		long num=0;
		final Random rn = new Random();
		
		int number = 0;

		for (int i = 0; i < 3; i++) {
			
			number = rn.nextInt();
			num = (num + (number < 0 ? -1 * number : number));
		}
		return num;
	}

	public List<EmployeeDto> getEmployeeList() {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		List<EmployeeDto> data = new ArrayList<>();
		
		try{  
		con = DBConnection.createConnection();  
		ps=con.prepareStatement(  
		    "select * from employeedetail");  
		rs = ps.executeQuery();
		while(rs.next()) {
			EmployeeDto employeeDto = new EmployeeDto();
			employeeDto.setEmpCode(rs.getInt("empCode"));
			employeeDto.setName(rs.getString("name"));
			employeeDto.setDateOfBirth(rs.getString("dateOfBirth"));
			employeeDto.setGender(rs.getString("gender"));
			employeeDto.setMaritalStatus(rs.getString("maritalStatus"));
			employeeDto.setMobNo(rs.getString("mobNo"));
			employeeDto.setAddress(rs.getString("address"));
			employeeDto.setDateOfBirth(rs.getString("dateOfHire"));
			employeeDto.setEmailId(rs.getString("emailId"));
			employeeDto.setEmpJobType(rs.getString("empJobType"));
			employeeDto.setDesignation(rs.getString("designation"));
			employeeDto.setStatus(rs.getString("status"));
			employeeDto.setEmergencyMobNO(rs.getString("emergencyMobNO"));
			employeeDto.setPanNo(rs.getString("panNo"));
			employeeDto.setAdharNo(rs.getString("adharNo"));
			employeeDto.setFilename(rs.getString("photo"));
			
			data.add(employeeDto);
		}
		               
		}catch(Exception e){
			System.out.println(e);
			data = new ArrayList<>();
		} finally{
			try {
				if (con != null && ps != null) {
					con.close();
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		return data;
	}

}


package com.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.admin.dto.JobsDto;
import com.admin.util.DBConnection;

public class JobsDao {

	public List<JobsDto> getjobsList() {
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<JobsDto> list=new ArrayList<JobsDto>();  
		
		try {
			 con = DBConnection.createConnection();
			 String sql ="SELECT jobcode, jobtitle, description,lastdate,updatedDate ,noofpost, status FROM jobdetail";
			 preparedStatement = con.prepareStatement(sql);
			 resultSet = preparedStatement.executeQuery();
			 while(resultSet.next()){
				 JobsDto jobsDto = new JobsDto();
				 jobsDto.setJobcode(resultSet.getString("jobcode"));
				 jobsDto.setJobtitle(resultSet.getString("jobtitle"));
				 jobsDto.setDescription(resultSet.getString("description"));
				 jobsDto.setLastdate(resultSet.getString("lastdate"));
				 jobsDto.setUpdatedDate(resultSet.getString("updatedDate"));
				 jobsDto.setNoofpost(resultSet.getString("noofpost"));
				 jobsDto.setStatus(resultSet.getInt("status"));
				 list.add(jobsDto);
			 }
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		return list;
	}


}

package com.admin.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.admin.dto.JobsDto;
import com.admin.util.DBConnection;

public class JobsPostDao {

	public boolean postJob(JobsDto data) throws IOException {

		boolean flag = false;
		
		String jcode = data.getJobcode();
		String jtitle = data.getJobtitle();
		String jdis = data.getDescription();
		String jlastdate = data.getLastdate();
		String jnopost = data.getNoofpost();
		String jupdatedDate = data.getUpdatedDate();

		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			con = DBConnection.createConnection();
			String query = "insert into jobdetail(jobcode,jobtitle, description, lastdate, updatedDate, noofpost, location, status)"
					+ "values (?,?,?,?,?,?,?,?)";
			preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, jcode);
			preparedStatement.setString(2, jtitle);
			preparedStatement.setString(3, jdis);
			preparedStatement.setString(4, jlastdate);
			preparedStatement.setString(5, jupdatedDate);
			preparedStatement.setString(6, jnopost);
			preparedStatement.setString(7, data.getLocation());
			preparedStatement.setInt(8, 1);

			int i = preparedStatement.executeUpdate();
			if (i != 0) {
				flag = true;
				return flag;
			} else {
				flag = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} finally {

			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return flag;
	}

	
	public boolean editJob(JobsDto data) throws IOException {

		boolean flag = false;
	
		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			con = DBConnection.createConnection();
			String query = "UPDATE jobdetail SET description = ?, lastdate = ?, updatedDate = ?,  noofpost =?,  status = ? WHERE  jobcode = ?";
			preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, data.getDescription());
			preparedStatement.setString(2, data.getLastdate());
			preparedStatement.setString(3, data.getUpdatedDate());
			preparedStatement.setString(4, data.getNoofpost());
			preparedStatement.setInt(5, data.getStatus());
			preparedStatement.setString(6, data.getJobcode());
			
			int i = preparedStatement.executeUpdate();
			if (i != 0) {
				flag = true;
				return flag;
			} else {
				flag = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} finally {

			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return flag;
	}

	
}

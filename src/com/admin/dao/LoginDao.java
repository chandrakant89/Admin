package com.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.admin.dto.LoginDto;
import com.admin.util.DBConnection;

public class LoginDao {
	public static LoginDto isValidate(LoginDto user) {
		Connection con = null;
		ResultSet rs = null;              
		PreparedStatement ps = null;
		
		LoginDto loginDto= new LoginDto(); 
		try{  
			con = DBConnection.createConnection();   
			ps = con.prepareStatement(  
		    "select userName, email, role  from logindetail where userName=? and password=?");  
		  
		ps.setString(1, user.getUserName());  
		ps.setString(2, user.getPassword());
		              
		rs = ps.executeQuery();
		while(rs.next()) {
			loginDto.setUserName(rs.getString("userName"));
			loginDto.setRole(rs.getString("role"));
			loginDto.setEmail(rs.getString("email"));
		}
		               
		}catch(Exception e){
			System.out.println(e);
		}  finally{
			try {
				if (con != null && ps != null) {
					con.close();
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		  
		return loginDto;  
		  
		}  

}

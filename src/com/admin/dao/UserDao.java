package com.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.admin.dto.UserDetailDto;
import com.admin.util.DBConnection;

public class UserDao {

	public List<UserDetailDto> getUserList() {
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<UserDetailDto> list=new ArrayList<UserDetailDto>();  
		
		try {
			 con = DBConnection.createConnection();
			 String sql ="SELECT userid, fname,lname,email,age,city,position,jobtype ,mobile,date,experience, application,jobcode, filepath FROM userdetail";
			 preparedStatement = con.prepareStatement(sql);
			 resultSet = preparedStatement.executeQuery();
			 while(resultSet.next()){
				 UserDetailDto userDetailDto = new UserDetailDto();
				 userDetailDto.setUserid(resultSet.getInt("userid"));
				 userDetailDto.setFname(resultSet.getString("fname"));
				 userDetailDto.setLname(resultSet.getString("lname"));
				 userDetailDto.setEmail(resultSet.getString("email"));
				 userDetailDto.setAge(resultSet.getInt("age"));
				 userDetailDto.setCity(resultSet.getString("city"));
				 userDetailDto.setPosition(resultSet.getString("position"));
				 userDetailDto.setJobtype(resultSet.getString("jobtype"));
				 userDetailDto.setMobile(resultSet.getString("mobile"));
				 userDetailDto.setExperience(resultSet.getString("experience"));
				 userDetailDto.setDate(resultSet.getDate("date"));
				 userDetailDto.setApplication(resultSet.getString("application"));
				 userDetailDto.setJobcode(resultSet.getString("jobcode"));
				 userDetailDto.setFilepath(resultSet.getString("filepath"));
				
				 
				 list.add(userDetailDto);
			 }
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		return list;
	}
	
	//Get User By Id and filepath
		public UserDetailDto getUserById(int userid,  String userfilepath) {
			
			Connection con = null;
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			UserDetailDto userDTO = new UserDetailDto();
			try {
				 con = DBConnection.createConnection();
				 String sql ="select userid, filepath from userdetail where userid=? and filepath =?";
				 preparedStatement = con.prepareStatement(sql);
				 preparedStatement.setInt(1, userid);
				 preparedStatement.setString(2, userfilepath);
				 resultSet = preparedStatement.executeQuery();
				 if(resultSet.next()){
					 userDTO.setUserid(resultSet.getInt("userid"));
					 userDTO.setFilepath(resultSet.getString("filepath"));
					 
					
				 }
				
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if (con != null && preparedStatement != null) {
						con.close();
						preparedStatement.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
			return userDTO;
			
		}
		
		// Get User By User Id
		public UserDetailDto getUserByUserId(int userid){
			Connection con = null;
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			UserDetailDto userDTO = new UserDetailDto();
			try {
				 con = DBConnection.createConnection();
				 String sql ="select userid, fname, lname, position from userdetail where userid=?";
				 preparedStatement = con.prepareStatement(sql);
				 preparedStatement.setInt(1, userid);
				 resultSet = preparedStatement.executeQuery();
				 if(resultSet.next()){
					 userDTO.setUserid(resultSet.getInt("userid"));
					 userDTO.setFname(resultSet.getString("fname"));
					 userDTO.setLname(resultSet.getString("lname"));
					 userDTO.setPosition(resultSet.getString("position"));
					}
				
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if (con != null && preparedStatement != null) {
						con.close();
						preparedStatement.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
			return userDTO;
		}
		
	}

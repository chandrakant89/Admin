package com.admin.dto;

public class ApplicationStatusDto {
	private int id;
	private String sendBy;
	private String applicantName;
	private String appliedPosition;
	private String sendTo;
	private String date;
	
	public ApplicationStatusDto() {
		super();
	}
	
	public ApplicationStatusDto(int id, String sendBy, String applicantName,
			String appliedPosition, String sendTo, String date) {
		super();
		this.id = id;
		this.sendBy = sendBy;
		this.applicantName = applicantName;
		this.appliedPosition = appliedPosition;
		this.sendTo = sendTo;
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSendBy() {
		return sendBy;
	}
	public void setSendBy(String sendBy) {
		this.sendBy = sendBy;
	}
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	public String getAppliedPosition() {
		return appliedPosition;
	}
	public void setAppliedPosition(String appliedPosition) {
		this.appliedPosition = appliedPosition;
	}
	public String getSendTo() {
		return sendTo;
	}
	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	
	

}

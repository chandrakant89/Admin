package com.admin.dto;

import javax.servlet.http.Part;


public class EmployeeDto {
	private int empCode;
	private String name;
	private String dateOfBirth;
	private String gender;
	private String maritalStatus;
	private String mobNo;
	private String address;
	private String dateOfHire;
	private String emailId;
	private String empJobType;
	private String designation;
	private String status;
	private String emergencyMobNO;
	private String panNo;
	private String adharNo;
	private Part photo;
	private String filename;
	
	public int getEmpCode() {
		return empCode;
	}
	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getMobNo() {
		return mobNo;
	}
	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDateOfHire() {
		return dateOfHire;
	}
	public void setDateOfHire(String dateOfHire) {
		this.dateOfHire = dateOfHire;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getEmpJobType() {
		return empJobType;
	}
	public void setEmpJobType(String empJobType) {
		this.empJobType = empJobType;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEmergencyMobNO() {
		return emergencyMobNO;
	}
	public void setEmergencyMobNO(String emergencyMobNO) {
		this.emergencyMobNO = emergencyMobNO;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getAdharNo() {
		return adharNo;
	}
	public void setAdharNo(String adharNo) {
		this.adharNo = adharNo;
	}
	public Part getPhoto() {
		return photo;
	}
	public void setPhoto(Part photo) {
		this.photo = photo;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	
	}
	
	
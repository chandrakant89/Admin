package com.admin.dto;

public class JobsDto {

	private String jobcode = null;
	private String jobtitle = null;
	private String description = null;
	private String lastdate = null;
	private String noofpost = null;
	private String updatedDate = null;
	private String location = null;
	private Integer status = null;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getJobcode() {
		return jobcode;
	}
	public void setJobcode(String jobcode) {
		this.jobcode = jobcode;
	}
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLastdate() {
		return lastdate;
	}
	public void setLastdate(String lastdate) {
		this.lastdate = lastdate;
	}
	public String getNoofpost() {
		return noofpost;
	}
	public void setNoofpost(String noofpost) {
		this.noofpost = noofpost;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "JobsDto [jobcode=" + jobcode + ", jobtitle=" + jobtitle + ", description=" + description + ", lastdate="
				+ lastdate + ", noofpost=" + noofpost + ", updatedDate=" + updatedDate + "]";
	}
	public JobsDto(String jobcode, String jobtitle, String description, String lastdate, String noofpost,
			String updatedDate) {
		super();
		this.jobcode = jobcode;
		this.jobtitle = jobtitle;
		this.description = description;
		this.lastdate = lastdate;
		this.noofpost = noofpost;
		this.updatedDate = updatedDate;
	}
	public JobsDto() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}

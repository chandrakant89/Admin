package com.admin.mailer;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mailer {

	public static void sendMail(String to, String bcc, String subject, String msg, String attachFile) {

		final String user = "info@nathap.in";
		final String pass = "g+hnt[;krmgtqc";

//Get the session object	
		Properties props = new Properties();
		props.put("mail.smtp.host", "nathap.in");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		});
//compose message
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.addRecipient(Message.RecipientType.BCC, new InternetAddress((bcc == null) ? null : bcc));
			message.setSubject(subject);

			// adds attachments
			if (attachFile != null && attachFile.length() > 0) {

				// creates message part
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(msg, "text/html");

				// creates multipart
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);

				String filepath = attachFile;
				MimeBodyPart attachPart = new MimeBodyPart();
				try {
					attachPart.attachFile(filepath);
				} catch (IOException ex) {
					ex.printStackTrace();
				}

				multipart.addBodyPart(attachPart);
				message.setContent(multipart);
			} else {
				message.setContent(msg, "text/html");
			}

			// send message
			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}
}

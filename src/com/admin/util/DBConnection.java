package com.admin.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	public static Connection createConnection() {
		Connection con = null;
		
		String url = Service.dburl;
		String username = Service.username;
		String password = Service.password;
				
		try {
		 try {
			Class.forName("com.mysql.jdbc.Driver");
		 	} catch (ClassNotFoundException e) {
			 e.printStackTrace();
		 	}
		  con = DriverManager.getConnection(url, username, password);
		 } catch (Exception e){
			 e.printStackTrace();
		 }
		   
		return con;
		
	}
}

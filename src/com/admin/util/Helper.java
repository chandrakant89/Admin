package com.admin.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.Part;

public class Helper {

	public static String getCurrentDate(){
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		return simpleDateFormat.format(date);
	}
	
	public static String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }
	
	public static long generateRandNumber(){
		long num=0;
		final Random rn = new Random();
		
		int number = 0;

		for (int i = 0; i < 3; i++) {
			
			number = rn.nextInt();
			num = (num + (number < 0 ? -1 * number : number));
		}
		return num;
	}
}

package com.nathap.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.admin.dto.JobsDto;
import com.nathap.dao.JobDAO;

public class JobListServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/*Job List*/
		
		JobDAO jobDAO = new JobDAO();
		List<JobsDto> list = new ArrayList<JobsDto>();
		list = jobDAO.getJobList();
		request.setAttribute("jobList", list);
		}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        doGet(req, res);
    }

}

package com.nathap.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.admin.util.Helper;
import com.nathap.dao.WriterDetailDao;
import com.nathap.dto.WriterDetailFormDto;

@MultipartConfig(fileSizeThreshold=1024*1024*160, // 2MB
maxFileSize=1024*1024*5,       // 10MB
maxRequestSize=1024*1024*10)	// 20MB
public class WriterDetailServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
	}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		boolean saveWriterDetail = false;
		
		WriterDetailDao writerDetailDao = new WriterDetailDao();
		
		String name = req.getParameter("name");
		String mobile = req.getParameter("mobile");
		String email = req.getParameter("email");
		String location =  req.getParameter("location");
		Part part = req.getPart("file");
		
		String fileName=Helper.extractFileName(part);
		
		
		WriterDetailFormDto writerDetailFormDto = new WriterDetailFormDto();
		
		writerDetailFormDto.setName(name);
		writerDetailFormDto.setMobile(mobile);
		writerDetailFormDto.setEmail(email);
		writerDetailFormDto.setLocation(location);
		writerDetailFormDto.setDate(Helper.getCurrentDate());
		writerDetailFormDto.setFile(part);
		writerDetailFormDto.setFilepath(fileName);
		writerDetailFormDto.setBio(req.getParameter("bio"));
		writerDetailFormDto.setUrl(req.getParameter("url"));
		writerDetailFormDto.setQualification(req.getParameter("qualification"));
		writerDetailFormDto.setTermsconditions((req.getParameter("termsconditions").equals("YES")) ? true : false);
		
		try {
			saveWriterDetail = writerDetailDao.saveWriterDetails(writerDetailFormDto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(saveWriterDetail) {
			req.setAttribute("message", "Your Request has been sent..");
			req.getRequestDispatcher("success.jsp").forward(req, res);
		}else {
			req.setAttribute("message", "Error");
			req.getRequestDispatcher("error.jsp").forward(req, res);
		}
	}
	
	
	
}

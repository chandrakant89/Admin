package com.nathap.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.Part;

import com.admin.mailer.Mailer;
import com.admin.util.DBConnection;
import com.admin.util.Service;
import com.admin.util.Helper;
import com.nathap.dto.WriterDetailFormDto;

public class WriterDetailDao  {

	public boolean saveWriterDetails(WriterDetailFormDto writerDetailFormDto) throws Exception {
		boolean flag = false;
		
		 File fileSaveDir=new File(Service.contentPath);
	        if(!fileSaveDir.exists()){
	            fileSaveDir.mkdir();
	        }
	        
		String name = writerDetailFormDto.getName();
		String mobile = writerDetailFormDto.getMobile();
		String email = writerDetailFormDto.getEmail();
		String location =  writerDetailFormDto.getLocation();
		String date = writerDetailFormDto.getDate();
		String filename =  writerDetailFormDto.getFilepath();
		Part part =  writerDetailFormDto.getFile();
		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			con = DBConnection.createConnection();
			String query = "insert into writerdetails(name, mobile, email, location, date, filepath, url, bio, qualification, termsconditions)"
					+ "values (?,?,?,?,?,?,?,?,?,?)";
			preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, mobile);
			preparedStatement.setString(3, email);
			preparedStatement.setString(4, location);
			preparedStatement.setString(5, date);
			
			String filepath= Service.contentPath + "/" + Helper.generateRandNumber() + filename ;
			preparedStatement.setString(6, filepath);
			preparedStatement.setString(7, writerDetailFormDto.getUrl());
			preparedStatement.setString(8, writerDetailFormDto.getBio());
			preparedStatement.setString(9, writerDetailFormDto.getQualification());
			preparedStatement.setBoolean(10, writerDetailFormDto.isTermsconditions());
			

			int i = preparedStatement.executeUpdate();
			if (i != 0) {
				flag = true;
				 part.write(filepath);
				 
				 if(flag) {
						String attachfile = filepath;
					
					//Send Mail
					
					//String to = req.getParameter("to");
					String to = "abhinav@nathap.in";
					String bcc = "hr@nathap.in";
					String subject = "Content Writer Detail";
					String message = "Get the content enclosed with the document.";
					String username = "Abhinav,";	
					
					String t = "The content of this message is confidential.If you have received it by mistake, please inform us by an email reply and then delete the message. It is forbidden to copy, forward, or in any way reveal the contents of this message to anyone. The integrity and security of this email cannot be guaranteed over the Internet. Therefore, the sender will not be held liable for any damage caused by the message.";
					String body = "<html><head><style type="+"text/css"+">"
							+".d{ font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; margin: 0 auto; max-width: 650px; color: #555;}"
							+".d1{height: 20px;}"
							+".d2{padding: 20px;}"
							+".d2 .img{text-align: center; border-bottom: 1px solid #555; padding: 10px;}"
							+".d3{text-align: center; padding: 30px; font-size: 18px;}"
							+".d4{padding: 10px 20px; font-size: 11px;}"
							+".d5{padding: 10px 0px 50px 0px;  border-bottom: 1px solid #ccc;}"
							+".d5 p span:nth-of-type(1){font-weight: 600;}"
							+"p{font-size: 13px;}"
							+"</style></head><body>"
							+"<div class="+"d"+">"
							+"<div class="+"d2"+"><div class="+"img"+"><a href="+"https://nathap.in/"+"><img src="+"https://nathap.in/images/logo.png"+"></a></div>"
							+"<div class="+"d3"+">Content Details</div>"
							+"<p>Hi "+username+"</p>"
							+"<p>"+message+"</p>"
							+"<div class="+"d5"+">"
							+"<p><span>Name: </span> <span>"+name+"</span> </p>"
//							+"<p><span>Job Code: </span> <span>"+null+"</span> </p>"
							+"<p><span>Email Id: </span> <span>"+email+"</span> </p>"
//							+"<p><span>Applied Position: </span><span>"+null+"</span> </p>"
							+"<p><span>Mobile No: </span> <span>"+mobile+"</span> </p>"
							+"</div></div><div class="+"d4"+">"
							+"<div>Copyrights � 2019 All Rights Reserved.</div>"
							+"<div>You subscribeed to our newsletter via our website, nathap.in</div>"
							+"</div><div class="+"d4"+">"+t+"</div></div></body></html>";
						
						Mailer.sendMail(to, bcc, subject, body, attachfile );
					}
				 
				return flag;
			} else {
				flag = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} finally {

			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return flag;
	
	
		
	}

	
}

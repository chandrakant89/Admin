package com.nathap.dto;

import javax.servlet.http.Part;

public class WriterDetailFormDto {
	
	private int writerid;
	private String name = null;
	private String mobile = null;
	private String email = null;
	private String location = null;
	private String date = null;
	private String filepath = null;
	private Part file;
	
	private String url = null;
	private String bio = null;
	private String qualification = null;
	private boolean termsconditions = true;
	
	public int getWriterid() {
		return writerid;
	}
	public void setWriterid(int writerid) {
		this.writerid = writerid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public Part getFile() {
		return file;
	}
	public void setFile(Part file) {
		this.file = file;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public boolean isTermsconditions() {
		return termsconditions;
	}
	public void setTermsconditions(boolean termsconditions) {
		this.termsconditions = termsconditions;
	}
	
	
	
	
}
